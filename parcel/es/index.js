import React from 'react'
import {
  render
} from 'react-dom'
import App from './app'

class AppContainer extends React.Component {
  state = {
    name: 'parcel test'
  }
  componentDidMount() {
    setTimeout(() => this.setState({
      name: 'parcel is nice'
    }), 3000);
  }
  render() {
    return <App name = {
      this.state.name
    }
    />
  }
}

render( <
  AppContainer / > ,
  document.getElementById('app')
)