const doSync = (sth, time) => new Promise(resolve => {
  setTimeout(() => {
    console.log(sth + '用了' + time + '毫秒')
    resolve()
  }, time);
})
const doAsync = (sth, time, cb) => {
  setTimeout(() => {
    console.log(sth + '用了' + time + '毫秒')
    cb && cb()
  }, time)
}
const doElse = (sth) => {
  console.log(sth)
}

const me = {
  doSync,
  doAsync
}
const she = {
  doSync,
  doAsync,
  doElse
}

;
(async () => {
  console.log('同步阻塞:她来到门口');
  await me.doSync('我刷牙', 1000)
  console.log('她啥也没干，一直等');
  await she.doSync('她洗脸', 2000)
  console.log('她去忙别的事');


  console.log('异步非阻塞:她来到门口设定闲置提醒');
  me.doAsync('我刷牙', 1000, () => {
    console.log('洗手间闲置提醒启动')
    she.doAsync('她洗脸', 2000)
  })
  she.doElse('她去忙别的了')
})()