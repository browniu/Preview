const cluster = require('cluster')
const cpus = require('os').cpus()

const masterProcess = () => {
  console.log(`一共有 ${cpus.length} 个核心`);
  console.log(`Master 主进程 ${process.pid} 启动`);

  for (let i = 0; i < cpus.length; i++) {
    console.log(`正在 Fork 子进程 ${i}`);
    cluster.fork()
  }
  process.exit()
}

const childProcess = () => {
  console.log(`Worker 子进程 ${process.pid} 启动并退出`)
  process.exit()
}

if (cluster.isMaster) {
  masterProcess()
} else {
  childProcess()
}

// 一共有 4 个核心
// Master 主进程 44240 启动
// 正在 Fork 子进程 0
// 正在 Fork 子进程 1
// 正在 Fork 子进程 2
// 正在 Fork 子进程 3