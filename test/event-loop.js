const {
  readFile,
  readFileSync
} = require('fs')

setImmediate(() => console.log('[阶段3.immediate] immediate回调1'))
setImmediate(() => console.log('[阶段3.immediate] immediate回调2'))
setImmediate(() => console.log('[阶段3.immediate] immediate回调3'))

Promise.resolve().then(() => {
  console.log('[阶段间隙.promise] resolve回调1')

  setImmediate(() => console.log('[阶段3.immediate] resolve回调1 > immediate回调4'))

})

readFile('../package.json', 'utf-8', data => {
  console.log('[阶段2.IO操作] 读取文件回调1');
  readFile('../video.mp4', 'utf-8', data => {
    console.log('[阶段2.IO操作] 读取文件回调1 > 读取文件回调2');
    setImmediate(() => console.log('[阶段3.immediate] 读取文件回调1 > 读取文件回调2 > immediate回调5'))
  })
})
setTimeout(() => console.log('[阶段1.定时器] 定时器回调1'), 0)
setTimeout(() => {
  console.log('[阶段1.定时器] 定时器回调2')
  process.nextTick(() => console.log('[阶段间隙.process] 定时器回调2 > nextTick回调1'))
}, 100)
setTimeout(() => console.log('[阶段1.定时器] 定时器回调3'), 200)

process.nextTick(() => console.log('[阶段间隙.process] nextTick回调2'))
process.nextTick(() => {
  console.log('[阶段间隙.process] nextTick回调3')
  process.nextTick(() => console.log('[阶段间隙.process] nextTick回调3 > nextTick回调4'))
})