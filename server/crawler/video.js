const puppeteer = require('puppeteer')

const base = `https://movie.douban.com/subject/`
const dbId = `26933677`
const videoBase = `https://movie.douban.com/trailer/232817/`

const sleep = time => new Promise(resolve => {
  setTimeout(resolve, time);
})

;
(async () => {
  console.log('start visit the target page');
  const browser = await puppeteer.launch({
    args: ['--no-sandbox'],
    dumpio: false
  })

  const page = await browser.newPage()
  await page.goto(base + dbId, {
    waitUntil: 'networkidle2'
  })

  await sleep(1000)
  const result = await page.evaluate(() => {
    var $ = window.$
    var it = $('.related-pic-video')

    if (it && it.length > 0) {
      var link = it.attr('href')
      var cover = it.css('background-image').split("\"")[1]
      return {
        link,
        cover
      }
    } else {}
    return {}
  })
  let video
  if (result.link) {
    await page.goto(result.link, {
      waitUntil: 'networkidle2'
    })
    await sleep(2000)
    video = await page.evaluate(() => {
      var $ = window.$
      var it = $('source')
      if (it && it.length > 0) {
        return it.attr('src')
      }
      return ''
    })
  }
  const data = {
    video,
    dbId,
    cover: result.cover
  }
  browser.close()
  process.send({
    data
  })
})()