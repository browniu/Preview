const puppeteer = require('puppeteer')
void(async () => {
  console.log('start visit the target page');
  const browser = await puppeteer.launch({
    headless: true
  })
  const page = await browser.newPage()
  await page.goto('https://www.zcool.com.cn')
  await page
    .mainFrame()
    .addScriptTag({
      url: 'https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js'
    })
  await page.waitFor(2000)

  const result = await page.evaluate(() => {
    var items = $('.card-box')
    var links = []

    if (items.length >= 1) {
      items.each((index, item) => {
        let it = $(item)
        let rate = Number(it.find('.statistics-tuijian').text())
        let title = it.find('.card-info a').text().replace(/\s+/g, '')
        let poster = it.find('img').attr('src')
        links.push({
          title,
          rate,
          poster
        })
      })
    }
    return links
  })
  console.log(result)
})()