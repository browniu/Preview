const qiniu = require('qiniu')
const nanoid = require('nanoid')
const config = require('../config')

const bucket = config.qiniu.bucket
const mac = new qiniu.auth.digest.Mac(config.qiniu.AK, config.qiniu.SK)
const cfg = new qiniu.conf.Config()
const client = new qiniu.rs.BucketManager(mac, cfg)

const uploadToQiniu = async (url, key) => {
  return new Promise((resolve, reject) => {
    client.fetch(url, bucket, key, (err, ret, info) => {
      if (err) {
        reject(err)
      } else {
        if (info.statusCode === 200) {
          resolve({
            key
          })
        } else {
          reject(info)
        }
      }
    })
  })
}

;
(async () => {
  let movies = [{
    dbId: '26933677',
    poster: 'https://img3.doubanio.com/view/photo/l_ratio_poster/public/p2521479463.jpg',
    cover: 'https://img3.doubanio.com/img/trailer/medium/2524749842.jpg?',
    video: 'http://vt1.doubanio.com/201807011701/bfe579446cd0e696010599b5db308334/view/movie/M/402320336.mp4'
  }]

  movies.map(async movie => {
    if (movie.video && !movie.key) {
      try {
        console.log('开始上传 video');
        let videoData = await uploadToQiniu(movie.video, nanoid() + '.mp4')
        console.log('开始上传 cover');
        let coverData = await uploadToQiniu(movie.cover, nanoid() + '.jpg')
        console.log('开始上传 poster');
        let posterData = await uploadToQiniu(movie.poster, nanoid() + '.jpg')
        if (videoData.key) {
          movie.videoKey = videoData.key
        }
        if (coverData.key) {
          movie.coverKey = videoData.key
        }
        if (posterData.key) {
          movie.posterKey = videoData.key
        }
        console.log(movie);
      } catch (err) {
        console.log(err);
      }
    }
  })
})()