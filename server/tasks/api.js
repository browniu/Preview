// http://api.douban.com/v2/movie/subject/1764796

const rp = require('request-promise-native')

async function fetchMovie(item) {
  const url = `http://api.douban.com/v2/movie/subject/${item.dbId}`

  const res = await rp(url)

  return res
};
(async () => {
  let movies = [{
      dbId: 27602222,
      title: '向往的生活 第二季',
      rate: 8.2,
      poster: 'https://img3.doubanio.com/view/photo/l_ratio_poster/public/p2520109024.jpg'
    },
    {
      dbId: 30158826,
      title: '家政夫三田园2',
      rate: 7.6,
      poster: 'https://img1.doubanio.com/view/photo/l_ratio_poster/public/p2518847649.jpg'
    }
  ]

  movies.map(async movie => {
    let movieData = await fetchMovie(movie)
    try {
      movieData = JSON.parse(movieData)

      console.log(movieData.title);
      console.log(movieData.summary);
    } catch (err) {
      console.log(err);
    }
    // console.log(movieData);
  })
})()